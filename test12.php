<?php
// transform.php
$xsl_filename = "catalog.xsl";
$xml_filename = "catalog.xml";
$doc = new DOMDocument();
$xsl = new XSLTProcessor();
$doc->load($xsl_filename);
$xsl->importStyleSheet($doc);
$doc->load($xml_filename);
echo $xsl->transformToXML($doc);
?>