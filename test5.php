<?php
// test5.php
$xmlstr = <<<XML
<?xml version='1.0'?>
<nations><nation id="th"><name>Thailand</name><location>Southeast Asia</location></nation>
<nation id="us"><name>The United States of America</name><location>North
America</location></nation>
</nations>
XML;
$xml = new SimpleXMLElement($xmlstr);
$xml->nation[1]->location = "America";
echo $xml->asXML("nations.xml");
?>