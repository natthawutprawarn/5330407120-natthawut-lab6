﻿<?php 

        $doc = new DOMDocument;
        $dom = new DomDocument('1.0','UTF-8'); 
        $url = "http://www.gotoknow.org/blogs/posts?format=rss";
        $doc->load($url);

        $rootNode = $doc->documentElement;
        $element = $rootNode->getElementsByTagName("item"); 

        echo "Reading from summaryBlogs.xml...<br>";
        $items = $dom->appendChild($dom->createElement('items'));

        foreach ($element AS $item) {
            
                $titleValue = $item->getElementsByTagName("title")->item(0)->nodeValue;
                $linkValue = $item->getElementsByTagName("link")->item(0)->nodeValue;
                $authorValue = $item->getElementsByTagName("author")->item(0)->nodeValue;
                
                $item = $items->appendChild($dom->createElement('item')); 

                $title = $item->appendChild($dom->createElement('title')); 
                $title->appendChild( $dom->createTextNode($titleValue));
                
                $link = $item->appendChild($dom->createElement('link'));
                $link->appendChild( $dom->createTextNode($linkValue));
                
                $author = $item->appendChild($dom->createElement('author')); 
                $author->appendChild( $dom->createTextNode($authorValue));

                echo $linkValue . $authorValue;
        }

        $dom->formatOutput = true;
        $summaryBlogs = $dom->saveXML();
        $dom->save('summaryBlogs.xml'); 

?>
